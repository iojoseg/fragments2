package mx.tecnm.misantla.fragments2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity(), MiFragment.nombreListener {

    var nombreActual: TextView? = null
    var addButton : Button? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

    nombreActual = findViewById(R.id.txtNombre)
        addButton = findViewById(R.id.btn_anadir)

        addButton?.setOnClickListener {
            val fragmentManager = supportFragmentManager
            // transaccion
            val fragmentTransation = fragmentManager.beginTransaction()

            val nuevoFragmento = MiFragment()
            fragmentTransation.add(R.id.container, nuevoFragmento)

            // dejar de ver el fragmento, regresar al estado anterior
            fragmentTransation.addToBackStack(null)
            
            fragmentTransation.commit()

        }

    }

    override fun obtenerNombre(nombre: String) {
        super.obtenerNombre(nombre)
        nombreActual?.text = nombre
    }
}