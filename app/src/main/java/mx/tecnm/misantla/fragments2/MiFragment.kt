package mx.tecnm.misantla.fragments2

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_mi.*
import java.lang.ClassCastException


class MiFragment : Fragment() {

     var boton : Button? = null
     var nombre: EditText? = null

    var listener : nombreListener? = null

    // nos sirve para utilizar el metodo inflate
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        // manipular la vista, para inicializar elementos
        val view = inflater.inflate(R.layout.fragment_mi, container, false)
        boton = view.findViewById(R.id.btn_Mostrar)
        nombre = view.findViewById(R.id.edtNombre)

        boton?.setOnClickListener {
            Toast.makeText(view.context, nombre?.text.toString(),Toast.LENGTH_SHORT).show()
          var nombreActual = nombre?.text.toString()
            listener?.obtenerNombre(nombreActual)
        }



        return view

    }

    // configurar una interface en nuestro fragmento
    // que nos permita exponer nuestra informacion

     interface nombreListener{
    fun obtenerNombre(nombre:String){
       }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try{
        listener = context as nombreListener
        }catch (ex: ClassCastException){
            throw ClassCastException(context.toString() + "Debes implementar la interface")
        }
    }

}